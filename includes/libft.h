/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 11:39:02 by aseo              #+#    #+#             */
/*   Updated: 2019/11/09 21:18:36 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <unistd.h>

typedef unsigned long long t_ull;

void				*ft_memset(void *b, int c, size_t len);
void				*ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memacpy(void *dst, const void *src, void *stopptr);
void				*ft_memmove(void *dst, const void *src, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
int					ft_strlen(const char *str);
int					ft_strnlen(const char *s, size_t maxlen);
int					ft_isalpha(int c);
int					ft_isalnum(int c);
int					ft_isdigit(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
void				ft_strcpy(const char *src, char *dst);
void				ft_strncpy(const char *src, char *dst, unsigned int n);
size_t				ft_strlcpy(const char *dst, const char *src, size_t size);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strnstr(const char *str, const char *to_find,
					size_t len);
int					ft_atoi(const char *str);
char				*ft_strdup(const char *src);
void				*ft_calloc(size_t count, size_t size);
char				*ft_substr(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s1, char const *set);
int					ft_ischarset(char c, char *charset);
char				*ft_itoa(int nb);
void				ft_strpush(char *str, char c);
char				**ft_splitstr(char *str, char *charset);
char				**ft_split(char const *s, char c);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr(char *str);
void				ft_putstr_fd(char *s, int fd);
void				ft_putnbr_fd(int nb, int fd);
void				ft_putendl_fd(char *s, int fd);
int					ft_strcchr(const char *str, char c, int *len);
int					ft_largest_3(int a, int b, int c);
int					get_next_line(int fd, char **line);
float				ft_atof(char *str);
void				free_split(char **sl);
int					ft_strcmp(char *s1, char *s2);

#endif
