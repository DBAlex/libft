/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 16:07:56 by aseo              #+#    #+#             */
/*   Updated: 2019/11/07 19:19:00 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 2048
# endif

typedef struct		s_glist
{
	char			*content;
	struct s_glist	*next;
	int				size;
}					t_glist;

typedef struct		s_gnl
{
	int				i;
	int				rint;
	int				nl;
	t_glist			*list;
	char			buff[BUFFER_SIZE];
}					t_gnl;

int					get_next_line(int fd, char **line);
int					next_nl(char *buff, int i, int rint);
t_glist				*new_elem(char *content, int size);
int					fill_list(t_gnl *g, int fd);
int					add_to_list(t_glist **list, char *buff, int size);
int					free_list(t_glist *list);
int					reset(t_gnl *g, int ret);

#endif
