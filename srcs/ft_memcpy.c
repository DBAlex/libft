/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 12:34:31 by aseo              #+#    #+#             */
/*   Updated: 2019/10/08 15:18:24 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char		*destptr;
	const char	*srcptr;

	if (src == NULL && dst == NULL)
		return (dst);
	destptr = dst;
	srcptr = src;
	while (n--)
	{
		*destptr++ = *srcptr++;
	}
	return (dst);
}
