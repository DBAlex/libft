/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 15:19:27 by aseo              #+#    #+#             */
/*   Updated: 2019/10/14 19:33:13 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	char		*d;
	const char	*s;
	size_t		sze;
	size_t		dlen;

	sze = size;
	d = dst;
	s = src;
	while (sze-- != 0 && *d != '\0')
		d++;
	dlen = d - dst;
	sze = size - dlen;
	if (size == dlen)
		return (dlen + ft_strlen(s));
	while (*s != '\0')
	{
		if (sze != 1)
		{
			*d++ = *s;
			sze--;
		}
		s++;
	}
	*d = '\0';
	return (dlen + (s - src));
}
