/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 20:56:46 by aseo              #+#    #+#             */
/*   Updated: 2019/11/07 20:57:03 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

float		ft_atof(char *str)
{
	float	n;
	int		i;
	int		k;
	int		sign;

	i = 0;
	n = 0;
	k = 0;
	sign = 1;
	if (!str)
		return (0);
	while (str[i] == ' ' || str[i] == '\v' || str[i] == '\t' || str[i] == '\r'
			|| str[i] == '\f' || str[i] == '\n')
		i++;
	sign = (str[i] == '-') ? -sign : sign;
	i = (str[i] == '-' || str[i] == '+') ? i + 1 : i;
	while (str[i] > 47 && str[i] < 58)
	{
		n = n * 10 + (str[i++] - 48);
		if (str[i] == '.')
			k = i++;
	}
	while (k != 0 && str[++k])
		sign = sign * 10;
	return (n / sign);
}
