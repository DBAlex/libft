/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 14:13:09 by aseo              #+#    #+#             */
/*   Updated: 2019/10/15 17:10:10 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strnstr(const char *str, const char *to_find, size_t len)
{
	unsigned int i;
	unsigned int o;

	i = 0;
	if (to_find[0] == '\0' || !str)
		return ((char *)str);
	while (str[i] != '\0' && i < len)
	{
		o = 0;
		while (str[i + o] == to_find[o] && (i + o) < len)
		{
			if (to_find[o + 1] == '\0')
				return ((char *)&str[i]);
			o++;
		}
		i++;
	}
	return (0);
}
