/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 18:01:36 by aseo              #+#    #+#             */
/*   Updated: 2019/10/23 13:51:52 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_strncpy(const char *src, char *dst, unsigned int n)
{
	char *s;

	s = (char *)src;
	while(n-- && *s)
		*dst++ = *s++;
}
