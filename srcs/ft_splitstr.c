/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_splitstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/12 18:02:22 by aseo              #+#    #+#             */
/*   Updated: 2019/11/07 21:06:49 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int		nb_strs(char *str, char *charset)
{
	int i;
	int nb;

	i = 0;
	nb = 0;
	while (ft_ischarset(str[i], charset))
		i++;
	if (!str[i])
		return (0);
	while (str[i])
	{
		if (ft_ischarset(str[i], charset) && !(ft_ischarset(str[i + 1],
		charset)) && (str[i + 1] != '\0'))
			nb++;
		i++;
	}
	return (nb + 1);
}

static int		strsize(int *offset, char *str, char *charset)
{
	int i;
	int a;

	a = 0;
	i = 0;
	while (str[i + *offset] && !(ft_ischarset(str[i + *offset], charset)))
		i++;
	while ((ft_ischarset(str[i + a + 1 + *offset], charset)))
		a++;
	*offset += i + a + 1;
	return (i + 1);
}

static void		split(char *str, char *charset, int nb, char **words)
{
	int i;
	int o;
	int offset;
	int tmp;
	int size;

	offset = 0;
	while (ft_ischarset(str[offset], charset))
		offset++;
	i = 0;
	while (i < nb)
	{
		o = -1;
		tmp = offset;
		size = strsize(&offset, str, charset);
		words[i] = malloc(sizeof(char) * size);
		while (++o < size - 1)
			words[i][o] = str[tmp + o];
		words[i][o] = '\0';
		i++;
	}
	words[nb] = 0;
}

char			**ft_splitstr(char *str, char *charset)
{
	int		nb;
	char	**words;

	nb = nb_strs(str, charset);
	words = NULL;
	words = malloc((sizeof(char *) * (nb + 1)));
	if (words == NULL)
		return (words);
	split(str, charset, nb, words);
	return (words);
}

void			free_split(char **sl)
{
	int	i;

	i = -1;
	while (sl[++i])
		free(sl[i]);
	free(sl);
}
