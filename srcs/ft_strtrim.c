/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 15:23:44 by aseo              #+#    #+#             */
/*   Updated: 2019/10/14 22:02:27 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strtrim(char const *s1, char const *set)
{
	int		i;
	int		o;
	int		start;
	char	*rstr;

	i = 0;
	while (s1[i] && ft_ischarset(s1[i], (char *)set))
		i++;
	if (!s1[i])
		return (ft_strdup(""));
	start = i;
	i = ft_strlen((char *)s1) - 1;
	while (ft_ischarset(s1[i], (char *)set))
		i--;
	if (!(rstr = malloc(sizeof(char) * (i - start + 2))))
		return (NULL);
	o = 0;
	while (o + start <= i)
	{
		rstr[o] = s1[o + start];
		o++;
	}
	rstr[o] = '\0';
	return (rstr);
}
