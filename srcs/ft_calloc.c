/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 16:14:44 by aseo              #+#    #+#             */
/*   Updated: 2019/10/15 17:23:36 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	unsigned char	*ptr;
	int				stop;

	stop = size * count;
	if (stop <= 0)
		return (ft_strdup(""));
	if (!(ptr = malloc(stop)))
		return (NULL);
	while (stop > 0)
	{
		ptr[--stop] = 0;
	}
	return (ptr);
}
