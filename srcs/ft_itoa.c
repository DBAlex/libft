/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 16:22:52 by aseo              #+#    #+#             */
/*   Updated: 2019/10/15 13:39:41 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	itoa_helper(int nb, char *str)
{
	if (nb > 9)
	{
		itoa_helper(nb / 10, str);
		itoa_helper(nb % 10, str);
	}
	else
	{
		ft_strpush(str, (char)(nb + 48));
	}
}

static int	getmallocsize(int nb)
{
	int r;

	r = (nb < 0) ? 1 : 0;
	if (nb < 0)
		nb = (nb == -2147483648) ? 2147483647 : -nb;
	if (nb < 10)
		return (r + 2);
	if (nb < 100)
		return (r + 3);
	if (nb < 1000)
		return (r + 4);
	if (nb < 10000)
		return (r + 5);
	if (nb < 100000)
		return (r + 6);
	if (nb < 1000000)
		return (r + 7);
	if (nb < 10000000)
		return (r + 8);
	if (nb < 100000000)
		return (r + 9);
	if (nb < 1000000000)
		return (r + 10);
	return (r + 11);
}

char		*ft_itoa(int nb)
{
	char	*rstr;

	if (!(rstr = malloc(getmallocsize(nb))))
		return (NULL);
	*rstr = '\0';
	if (nb < 0)
	{
		ft_strpush(rstr, '-');
		if (nb == -2147483648)
		{
			itoa_helper(214748364, rstr);
			ft_strpush(rstr, '8');
			return (rstr);
		}
		nb = -nb;
	}
	itoa_helper(nb, rstr);
	return (rstr);
}
