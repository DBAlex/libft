/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 12:17:12 by aseo              #+#    #+#             */
/*   Updated: 2019/10/15 13:52:04 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	unsigned int	i;
	char			*rstr;

	i = ft_strlen(s);
	if (i <= start)
		return (ft_strdup(""));
	i = 0;
	if (!(rstr = malloc(sizeof(char) * len + 1)))
		return (NULL);
	while (s[i + start] && i < len)
	{
		rstr[i] = s[i + start];
		i++;
	}
	rstr[i] = '\0';
	return (rstr);
}
