/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 17:48:38 by aseo              #+#    #+#             */
/*   Updated: 2019/10/08 15:23:36 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	char		*destptr;
	const char	*srcptr;

	if (src == NULL && dst == NULL)
		return (dst);
	if (dst < src)
	{
		destptr = dst;
		srcptr = src;
		while (n--)
			*destptr++ = *srcptr++;
	}
	else
	{
		destptr = dst + n - 1;
		srcptr = src + n - 1;
		while (n--)
			*destptr-- = *srcptr--;
	}
	return (dst);
}
