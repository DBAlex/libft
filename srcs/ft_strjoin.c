/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 12:55:15 by aseo              #+#    #+#             */
/*   Updated: 2019/10/09 16:21:02 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	size;
	unsigned int	i;
	unsigned int	o;
	char			*rstr;

	size = ft_strlen((char*)s1) + ft_strlen((char*)s2);
	if (!(rstr = malloc(size + 1)))
		return (NULL);
	i = 0;
	while (s1[i])
	{
		rstr[i] = s1[i];
		i++;
	}
	o = 0;
	while (s2[o])
	{
		rstr[i + o] = s2[o];
		o++;
	}
	rstr[i + o] = '\0';
	return (rstr);
}
