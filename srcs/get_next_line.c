/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 16:07:52 by aseo              #+#    #+#             */
/*   Updated: 2019/11/09 16:17:09 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <unistd.h>
#include <stdlib.h>

int		add_to_list(t_glist **list, char *buff, int size)
{
	char	*ptr;
	t_glist	*lptr;
	int		i;

	if (!(ptr = malloc(size)))
		return (0);
	i = -1;
	while (++i < size)
		ptr[i] = buff[i];
	if (*list == NULL)
	{
		if (!(*list = new_elem(ptr, size)))
			return (0);
	}
	else
	{
		lptr = *list;
		while (lptr->next)
			lptr = lptr->next;
		if (!(lptr->next = new_elem(ptr, size)))
			return (0);
	}
	return (1);
}

void	join(t_glist *ptr, char **str, int *o)
{
	int		i;
	t_glist	*bptr;

	while (ptr)
	{
		i = -1;
		while (++i < ptr->size)
			(*str)[i + *o] = ptr->content[i];
		*o += i;
		free(ptr->content);
		bptr = ptr;
		ptr = ptr->next;
		free(bptr);
	}
}

/*
** list_join()
*/

char	*ljn(char *buff, t_glist *list, int rpos)
{
	char	*str;
	int		msize;
	t_glist	*ptr;
	int		i;
	int		o;

	ptr = list;
	msize = 0;
	o = 0;
	while (ptr)
	{
		msize += ptr->size;
		ptr = ptr->next;
	}
	if (!(str = malloc(msize + rpos + 1)))
		return (NULL);
	join(list, &str, &o);
	i = -1;
	while (++i < rpos)
		str[i + o] = buff[i];
	str[i + o] = '\0';
	return (str);
}

/*
** alloc_substr()
*/

char	*sbstr(int nl, int *i, char *buff)
{
	int		o;
	char	*ptr;

	o = 0;
	if (!(ptr = malloc(nl - *i)))
		return (NULL);
	(*i)++;
	while (*i < nl)
	{
		ptr[o] = buff[*i];
		(*i)++;
		o++;
	}
	ptr[o] = 0;
	return (ptr);
}

int		get_next_line(int fd, char **line)
{
	static t_gnl	g = {-1, -1, -1, 0, {0}};

	g.list = NULL;
	if (g.i != -1 && (g.nl = next_nl(g.buff, g.i + 1, g.rint)) == -1
	&& (g.i != g.rint - 1))
	{
		g.i++;
		add_to_list(&g.list, &g.buff[g.i], BUFFER_SIZE - g.i);
	}
	if ((g.rint == BUFFER_SIZE && g.nl == -1) || g.rint == -1)
		g.rint = read(fd, g.buff, BUFFER_SIZE);
	if ((g.rint == 0 || g.rint == -1) && !g.list)
		return (reset(&g, g.rint));
	if (!fill_list(&g, fd))
		return (free_list(g.list));
	*line = (g.nl == -1) ? ljn(g.buff, g.list, g.i) : sbstr(g.nl, &g.i, g.buff);
	if ((g.i == g.rint) && g.rint != BUFFER_SIZE)
		return (reset(&g, 0));
	return ((*line == NULL) ? -1 : 1);
}
