/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/14 15:03:02 by aseo              #+#    #+#             */
/*   Updated: 2019/11/07 19:17:06 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdlib.h>
#include <unistd.h>

int		next_nl(char *buff, int i, int rint)
{
	while (i < BUFFER_SIZE && buff[i] != '\n' && i < rint)
		i++;
	if (i == BUFFER_SIZE)
		return (-1);
	return (i);
}

t_glist	*new_elem(char *content, int size)
{
	t_glist	*ptr;

	if (!(ptr = malloc(sizeof(t_glist))))
		return (NULL);
	ptr->content = content;
	ptr->next = NULL;
	ptr->size = size;
	return (ptr);
}

int		fill_list(t_gnl *g, int fd)
{
	while (g->nl == -1)
	{
		g->i = 0;
		if ((g->i = next_nl(g->buff, g->i, g->rint)) == -1)
		{
			if (!(add_to_list(&g->list, g->buff, BUFFER_SIZE)))
				return (0);
		}
		else
			break ;
		g->rint = read(fd, g->buff, BUFFER_SIZE);
	}
	return (1);
}

int		free_list(t_glist *list)
{
	t_glist *ptr;
	t_glist *tmp;

	ptr = list;
	while (ptr)
	{
		if (ptr->content)
			free(ptr->content);
		tmp = ptr;
		free(ptr);
		ptr = tmp->next;
	}
	return (-1);
}

int		reset(t_gnl *g, int ret)
{
	g->i = -1;
	g->rint = -1;
	g->nl = -1;
	g->list = NULL;
	return (ret);
}
