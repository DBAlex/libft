/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 12:39:27 by aseo              #+#    #+#             */
/*   Updated: 2019/10/08 15:26:38 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strchr(const char *s, int c)
{
	const char *ptr;

	ptr = s;
	while (*ptr && *ptr != c)
		ptr++;
	return ((*ptr == c) ? (char *)ptr : 0);
}
