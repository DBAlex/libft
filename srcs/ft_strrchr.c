/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 12:39:27 by aseo              #+#    #+#             */
/*   Updated: 2019/10/08 15:22:35 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrchr(const char *s, int c)
{
	const char	*ptr;
	char		*foundptr;

	ptr = s;
	if (c == 0)
	{
		while (*ptr)
			ptr++;
		return ((char *)ptr);
	}
	foundptr = 0;
	while (*ptr)
	{
		if (*ptr == c)
			foundptr = (char *)ptr;
		ptr++;
	}
	return (foundptr);
}
