/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memacpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 12:34:31 by aseo              #+#    #+#             */
/*   Updated: 2019/10/20 12:28:32 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	*ft_memacpy(void *dst, const void *src, void *stopptr)
{
	char		*destptr;
	const char	*srcptr;

	if (src == NULL || dst == NULL || stopptr == NULL)
		return (dst);
	destptr = dst;
	srcptr = src;
	while (srcptr != stopptr)
		*destptr++ = *srcptr++;
	return (destptr);
}
