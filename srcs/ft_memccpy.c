/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 16:41:37 by aseo              #+#    #+#             */
/*   Updated: 2019/10/08 15:18:53 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char		*destptr;
	const char	*srcptr;

	destptr = dst;
	srcptr = src;
	while (n--)
	{
		*destptr = *srcptr;
		if (*srcptr == (char)c)
			return (++destptr);
		destptr++;
		srcptr++;
	}
	return (NULL);
}
